# nanoiec

In this Project i established a simple vc1541 in an arduino nano. Most of the code is taken from
https://github.com/Larswad/uno2iec
so i like to thank Lars Wadefalk very much for his great work.

All that it does at this time is showing a directory when giving a
load"$",8

the files in that directory are not accessible and it has some garbage in it. But in general,
the data is provided by the arduino nano. Please have a look at the serial console in the arduino ide.

So if you want to build it you will have to connect your floppy cable of your commodore c64
to the following pins of your arduino nano:
#define DEFAULT_ATN_PIN 5
#define DEFAULT_DATA_PIN 3
#define DEFAULT_CLOCK_PIN 4

these three connections are necessary.

The reset pin of your arduino must be connected to +5V to not having any reset signals.
I have made this connection via a 10k resistor, but i even tried it by direct connection this should even work.

