#include "global_defines.h"
#include "iec_driver.h"
#include "interface.h"

// Pin 13 has a LED connected on most Arduino boards.
const byte ledPort = 13;
const byte numBlinks = 4;

// The global IEC handling singleton:
static IEC iec(8);
static Interface iface(iec);

static ulong lastMillis = 0;

void setup()
{
	// Initialize serial and wait for port to open:
	COMPORT.begin(DEFAULT_BAUD_RATE);
	COMPORT.setTimeout(SERIAL_TIMEOUT_MSECS);
	COMPORT.write("marke0");
	COMPORT.flush();

	// set all digital pins in a defined state.
	iec.init();

	COMPORT.write("marke1");
	iface.handleAWSaveFile();
	COMPORT.write("marke2");

	lastMillis = millis();
} // setup


void loop()
{
	//COMPORT.write("in loop\n");
	if(IEC::ATN_RESET == iface.handler()) {
		// Wait for it to get out of reset.
		while(IEC::ATN_RESET == iface.handler());
	}
} // loop


